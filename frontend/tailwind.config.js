module.exports = {
  purge: { content: ["./public/**/*.html", "./src/**/*.vue", "./*.config.js"] },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        inter: ["Inter", "sans-serif"]
      }
    }
  },
  variants: {
    extend: {
      opacity: ["active"]
    }
  },
  plugins: []
};
