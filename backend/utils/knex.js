const connection_string =
  process.env.PG_CONNECTION_STRING || "postgresql://postgres:postgres@localhost:5432/quicksplain";

const dbConfig = {
  client: "pg",
  connection: connection_string,
  useNullAsDefault: true,
};
const knex = require("knex")(dbConfig);

module.exports = knex;
