const bookshelf = require("../utils/bookshelf");

const Guess = bookshelf.model("Guess", {
  tableName: "guess",
  turn() {
    return this.belongsTo("Turn");
  },
  keyword() {
    return this.belongsTo("Keyword");
  },
});

module.exports = Guess;
