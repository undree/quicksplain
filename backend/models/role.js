const bookshelf = require("../utils/bookshelf");

const Role = bookshelf.model("Role", {
  tableName: "role",
  users() {
    return this.hasMany("User");
  },
});

module.exports = Role;
