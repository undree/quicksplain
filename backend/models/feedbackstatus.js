const bookshelf = require("../utils/bookshelf");

const FeedbackStatus = bookshelf.model("FeedbackStatus", {
  tableName: "feedbackstatus",
  feedbacks() {
    return this.hasMany("Feedback");
  },
});

module.exports = FeedbackStatus;
