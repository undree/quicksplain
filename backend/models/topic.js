const bookshelf = require("../utils/bookshelf");

const Topic = bookshelf.model("Topic", {
  tableName: "topic",
  category() {
    return this.belongsTo("Category");
  },
  keywords() {
    return this.hasMany("Keyword").query((qb) => {
      qb.select("*");
      qb.orderBy("name");
    });
  },
  enabledKeywords() {
    return this.keywords().query("where", "enabled", true);
  },
  randomEnabledKeywords() {
    return this.keywords().query((qb) => {
      qb.select("*");
      qb.where("enabled", true);
      qb.orderByRaw("random()");
    });
  },
});

module.exports = Topic;
