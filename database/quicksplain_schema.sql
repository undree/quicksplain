--
-- PostgreSQL database dump
--

-- Dumped from database version 12.7 (Ubuntu 12.7-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.7 (Ubuntu 12.7-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id integer NOT NULL,
    name text NOT NULL,
    enabled boolean NOT NULL
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- Name: feedback; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.feedback (
    id integer NOT NULL,
    feedbacktype_id integer NOT NULL,
    author_id integer,
    reviewer_id integer,
    feedbackstatus_id integer NOT NULL,
    turn_id integer,
    keyword_id integer,
    topic_id integer,
    comment text,
    oldname text,
    newname text,
    "timestamp" timestamp without time zone NOT NULL
);


ALTER TABLE public.feedback OWNER TO postgres;

--
-- Name: feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.feedback_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.feedback_id_seq OWNER TO postgres;

--
-- Name: feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.feedback_id_seq OWNED BY public.feedback.id;


--
-- Name: feedbackstatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.feedbackstatus (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.feedbackstatus OWNER TO postgres;

--
-- Name: feedbackstatus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.feedbackstatus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.feedbackstatus_id_seq OWNER TO postgres;

--
-- Name: feedbackstatus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.feedbackstatus_id_seq OWNED BY public.feedbackstatus.id;


--
-- Name: feedbacktype; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.feedbacktype (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.feedbacktype OWNER TO postgres;

--
-- Name: feedbacktype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.feedbacktype_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.feedbacktype_id_seq OWNER TO postgres;

--
-- Name: feedbacktype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.feedbacktype_id_seq OWNED BY public.feedbacktype.id;


--
-- Name: guess; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.guess (
    id integer NOT NULL,
    turn_id integer NOT NULL,
    keyword_id integer NOT NULL,
    guessed boolean NOT NULL
);


ALTER TABLE public.guess OWNER TO postgres;

--
-- Name: guess_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.guess_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guess_id_seq OWNER TO postgres;

--
-- Name: guess_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.guess_id_seq OWNED BY public.guess.id;


--
-- Name: keyword; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.keyword (
    id integer NOT NULL,
    name text NOT NULL,
    topic_id integer NOT NULL,
    enabled boolean NOT NULL
);


ALTER TABLE public.keyword OWNER TO postgres;

--
-- Name: keyword_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.keyword_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.keyword_id_seq OWNER TO postgres;

--
-- Name: keyword_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.keyword_id_seq OWNED BY public.keyword.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: topic; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.topic (
    id integer NOT NULL,
    name text NOT NULL,
    category_id integer NOT NULL,
    enabled boolean NOT NULL
);


ALTER TABLE public.topic OWNER TO postgres;

--
-- Name: topic_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.topic_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.topic_id_seq OWNER TO postgres;

--
-- Name: topic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.topic_id_seq OWNED BY public.topic.id;


--
-- Name: topicchoice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.topicchoice (
    id integer NOT NULL,
    turn_id integer NOT NULL,
    topic_id integer NOT NULL,
    chosen boolean NOT NULL
);


ALTER TABLE public.topicchoice OWNER TO postgres;

--
-- Name: topicchoice_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.topicchoice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.topicchoice_id_seq OWNER TO postgres;

--
-- Name: topicchoice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.topicchoice_id_seq OWNED BY public.topicchoice.id;


--
-- Name: turn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.turn (
    id integer NOT NULL,
    topic_id integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    random_topic boolean NOT NULL
);


ALTER TABLE public.turn OWNER TO postgres;

--
-- Name: turn_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.turn_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turn_id_seq OWNER TO postgres;

--
-- Name: turn_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.turn_id_seq OWNED BY public.turn.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username text NOT NULL,
    email text,
    password_hash text NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- Name: feedback id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback ALTER COLUMN id SET DEFAULT nextval('public.feedback_id_seq'::regclass);


--
-- Name: feedbackstatus id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedbackstatus ALTER COLUMN id SET DEFAULT nextval('public.feedbackstatus_id_seq'::regclass);


--
-- Name: feedbacktype id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedbacktype ALTER COLUMN id SET DEFAULT nextval('public.feedbacktype_id_seq'::regclass);


--
-- Name: guess id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.guess ALTER COLUMN id SET DEFAULT nextval('public.guess_id_seq'::regclass);


--
-- Name: keyword id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keyword ALTER COLUMN id SET DEFAULT nextval('public.keyword_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: topic id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topic ALTER COLUMN id SET DEFAULT nextval('public.topic_id_seq'::regclass);


--
-- Name: topicchoice id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topicchoice ALTER COLUMN id SET DEFAULT nextval('public.topicchoice_id_seq'::regclass);


--
-- Name: turn id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turn ALTER COLUMN id SET DEFAULT nextval('public.turn_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: feedback; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.feedback (id, feedbacktype_id, author_id, reviewer_id, feedbackstatus_id, turn_id, keyword_id, topic_id, comment, oldname, newname, "timestamp") FROM stdin;
\.


--
-- Data for Name: feedbackstatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.feedbackstatus (id, name) FROM stdin;
1	New
2	Rejected
3	Accepted
4	Invalid
\.


--
-- Data for Name: feedbacktype; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.feedbacktype (id, name) FROM stdin;
1	Comment
2	Add keyword
3	Remove keyword
4	Modify keyword
\.


--
-- Data for Name: guess; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.guess (id, turn_id, keyword_id, guessed) FROM stdin;
\.


--
-- Data for Name: keyword; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.keyword (id, name, topic_id, enabled) FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role (id, name) FROM stdin;
1	Admin
2	Reviewer
3	Pro
4	Regular
\.


--
-- Data for Name: topic; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.topic (id, name, category_id, enabled) FROM stdin;
\.


--
-- Data for Name: topicchoice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.topicchoice (id, turn_id, topic_id, chosen) FROM stdin;
\.


--
-- Data for Name: turn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.turn (id, topic_id, "timestamp", random_topic) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, username, email, password_hash, role_id) FROM stdin;
1	admin	\N	$2b$12$cNxJy7we3395COJO26ReBO3NVKgWNr7wDsTTzVmlTazdPpU1GzhSi	1
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_id_seq', 8, true);


--
-- Name: feedback_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.feedback_id_seq', 1, false);


--
-- Name: feedbackstatus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.feedbackstatus_id_seq', 4, true);


--
-- Name: feedbacktype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.feedbacktype_id_seq', 4, true);


--
-- Name: guess_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.guess_id_seq', 1, false);


--
-- Name: keyword_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.keyword_id_seq', 1, false);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_id_seq', 4, true);


--
-- Name: topic_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.topic_id_seq', 1, false);


--
-- Name: topicchoice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.topicchoice_id_seq', 1, false);


--
-- Name: turn_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.turn_id_seq', 1, false);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 1, true);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: feedback feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (id);


--
-- Name: feedbackstatus feedbackstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedbackstatus
    ADD CONSTRAINT feedbackstatus_pkey PRIMARY KEY (id);


--
-- Name: feedbacktype feedbacktype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedbacktype
    ADD CONSTRAINT feedbacktype_pkey PRIMARY KEY (id);


--
-- Name: guess guess_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.guess
    ADD CONSTRAINT guess_pkey PRIMARY KEY (id);


--
-- Name: keyword keyword_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keyword
    ADD CONSTRAINT keyword_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: topic topic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topic
    ADD CONSTRAINT topic_pkey PRIMARY KEY (id);


--
-- Name: topicchoice topicchoice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topicchoice
    ADD CONSTRAINT topicchoice_pkey PRIMARY KEY (id);


--
-- Name: turn turn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turn
    ADD CONSTRAINT turn_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: category_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX category_name ON public.category USING btree (name);


--
-- Name: feedback_author_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX feedback_author_id ON public.feedback USING btree (author_id);


--
-- Name: feedback_feedbackstatus_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX feedback_feedbackstatus_id ON public.feedback USING btree (feedbackstatus_id);


--
-- Name: feedback_feedbacktype_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX feedback_feedbacktype_id ON public.feedback USING btree (feedbacktype_id);


--
-- Name: feedback_keyword_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX feedback_keyword_id ON public.feedback USING btree (keyword_id);


--
-- Name: feedback_reviewer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX feedback_reviewer_id ON public.feedback USING btree (reviewer_id);


--
-- Name: feedback_topic_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX feedback_topic_id ON public.feedback USING btree (topic_id);


--
-- Name: feedback_turn_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX feedback_turn_id ON public.feedback USING btree (turn_id);


--
-- Name: guess_keyword_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX guess_keyword_id ON public.guess USING btree (keyword_id);


--
-- Name: guess_turn_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX guess_turn_id ON public.guess USING btree (turn_id);


--
-- Name: keyword_name_topic_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX keyword_name_topic_id ON public.keyword USING btree (name, topic_id);


--
-- Name: keyword_topic_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX keyword_topic_id ON public.keyword USING btree (topic_id);


--
-- Name: topic_category_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX topic_category_id ON public.topic USING btree (category_id);


--
-- Name: topic_name_category_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX topic_name_category_id ON public.topic USING btree (name, category_id);


--
-- Name: topicchoice_topic_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX topicchoice_topic_id ON public.topicchoice USING btree (topic_id);


--
-- Name: topicchoice_turn_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX topicchoice_turn_id ON public.topicchoice USING btree (turn_id);


--
-- Name: turn_topic_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX turn_topic_id ON public.turn USING btree (topic_id);


--
-- Name: user_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX user_email ON public."user" USING btree (email);


--
-- Name: user_role_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_role_id ON public."user" USING btree (role_id);


--
-- Name: user_username; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX user_username ON public."user" USING btree (username);


--
-- Name: feedback feedback_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_author_id_fkey FOREIGN KEY (author_id) REFERENCES public."user"(id);


--
-- Name: feedback feedback_feedbackstatus_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_feedbackstatus_id_fkey FOREIGN KEY (feedbackstatus_id) REFERENCES public.feedbackstatus(id);


--
-- Name: feedback feedback_feedbacktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_feedbacktype_id_fkey FOREIGN KEY (feedbacktype_id) REFERENCES public.feedbacktype(id);


--
-- Name: feedback feedback_keyword_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_keyword_id_fkey FOREIGN KEY (keyword_id) REFERENCES public.keyword(id);


--
-- Name: feedback feedback_reviewer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_reviewer_id_fkey FOREIGN KEY (reviewer_id) REFERENCES public."user"(id);


--
-- Name: feedback feedback_topic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_topic_id_fkey FOREIGN KEY (topic_id) REFERENCES public.topic(id);


--
-- Name: feedback feedback_turn_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_turn_id_fkey FOREIGN KEY (turn_id) REFERENCES public.turn(id);


--
-- Name: guess guess_keyword_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.guess
    ADD CONSTRAINT guess_keyword_id_fkey FOREIGN KEY (keyword_id) REFERENCES public.keyword(id);


--
-- Name: guess guess_turn_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.guess
    ADD CONSTRAINT guess_turn_id_fkey FOREIGN KEY (turn_id) REFERENCES public.turn(id);


--
-- Name: keyword keyword_topic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keyword
    ADD CONSTRAINT keyword_topic_id_fkey FOREIGN KEY (topic_id) REFERENCES public.topic(id);


--
-- Name: topic topic_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topic
    ADD CONSTRAINT topic_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.category(id);


--
-- Name: topicchoice topicchoice_topic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topicchoice
    ADD CONSTRAINT topicchoice_topic_id_fkey FOREIGN KEY (topic_id) REFERENCES public.topic(id);


--
-- Name: topicchoice topicchoice_turn_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.topicchoice
    ADD CONSTRAINT topicchoice_turn_id_fkey FOREIGN KEY (turn_id) REFERENCES public.turn(id);


--
-- Name: turn turn_topic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turn
    ADD CONSTRAINT turn_topic_id_fkey FOREIGN KEY (topic_id) REFERENCES public.topic(id);


--
-- Name: user user_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- PostgreSQL database dump complete
--

